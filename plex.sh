#!/bin/bash

#config file loading
#. /plexconfiguration.conf
scriptversion=1.0.1
configfilelocation=/root/.config/plexscript/plexscript.conf
cronreboottime=
cronupdateappsostime="0 6 * * *"
newtimezone=
newhostname=
rcloneservice1='rclone-plex-wasabi.service'
rclonemount=
rclonetarget=
rcloneservice2=
rclonemount2=
rclonetarget2=
upgradescript=false
mediadirectory1=0
mediadirectory2=0
mediadirectory3=0
mediadirectory4=0
mediadirectory5=0
#--------------------------------------------------------------------------------------------------------------------
# Start time of the script
starttime=$(date +%Y%m%d-%H%M%S)
#--------------------------------------------------------------------------------------------------------------------
function functionUpdateScript() {
    if [[ $upgradescript = true ]];
        then
            if [[ ! -f ./plex.sh ]]; 
                then
                    echo "No Script to remove"
                    echo "-------------------"
                else
                    echo "Removing old script"
                    echo "-------------------"
                    sudo rm ./plex.sh
                fi
        fi
    if [[ ! -f ./plex.sh ]]; 
    then
        wget -q https://gitlab.com/mkolakowski/plex/raw/master/plex.sh
        if [[ $? -ne 0 ]];
            then
                echo "There was an error downloading the latest release"
                echo "-------------------------------------------------"
            else
                echo "Latest release Downloaded"
                echo "-------------------------"
            fi
        sudo chmod u+x ./plex.sh
 #    else
 #        echo "No changes made to/plex.sh"
 #        echo "---------------------------"
    fi
}
#--------------------------------------------------------------------------------------------------------------------
function functionupdateosapps () {
    #tested 2019-10-21 - mkolakowski - works as intended
    echo "Performing updates"
    echo "------------------"
    apt-get update && apt-get upgrade -f --assume-yes
    functionUpdateScript
}
#--------------------------------------------------------------------------------------------------------------------
function functionSetHostname () {
    echo "Changing hostname to $newhostname"
    echo "--------------------"
    sudo hostname "$newhostname"
    sudo hostnamectl set-hostname "$newhostname"
}
#--------------------------------------------------------------------------------------------------------------------
# Reads config and asks user if they want to modify it
function functionPrintConfig() {
    echo "      configfilelocation:    $configfilelocation"
    echo "      cronreboottime:        $cronreboottime"
    echo "      cronupdateappsostime:  $cronupdateappsostime"
    echo "      newtimezone:           $newtimezone"
    echo "      newhostname:           $newhostname"
    echo "      rcloneservice1:        $rcloneservice1"
    echo "      rclonemount:           $rclonemount"
    echo "      rclonetarget:          $rclonetarget"
    echo "      rcloneservice2:        $rcloneservice2"
    echo "      rclonemount2:          $rclonemount2"
    echo "      rclonetarget2:         $rclonetarget2"
    echo "      mediadirectory1:       $mediadirectory1"
    echo "      mediadirectory2:       $mediadirectory2"
    echo "      mediadirectory3:       $mediadirectory3"
    echo "      mediadirectory4:       $mediadirectory4"
    echo "      mediadirectory4:       $mediadirectory5"
}
#--------------------------------------------------------------------------------------------------------------------
function functionLoadConfigVariables () {
    #tells the program to refrence this file
    . $configfilelocation
    echo "Loading Configiguration from file"
    echo "---------------------------------"
    configfilelocation=$confconfigfilelocation
    cronreboottime=$confcronreboottime
    cronupdateappsostime=$confcronupdateappsostime
    newtimezone=$confnewtimezone
    newhostname=$varnewhostname
    rcloneservice1=$varrcloneservice1
    rclonemount=$varrclonemount1
    rclonetarget=$varrclonetarget1
    rcloneservice2=$varrcloneservice2
    rclonemount2=$varrclonemount2
    rclonetarget2=$varrclonetarget2
    mediadirectory1=$varmediadirectory1
    mediadirectory2=$varmediadirectory2
    mediadirectory3=$varmediadirectory3
    mediadirectory4=$varmediadirectory4
    mediadirectory5=$varmediadirectory5
    echo " "
}
#--------------------------------------------------------------------------------------------------------------------
# Reads config and asks user if they want to modify it
function functionReadConfig() {
    functionLoadConfigVariables
    functionPrintConfig
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo "!! Please verify that the config is correct before proceding  !!"
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    read -p "Do you want to edit the config before proceding? (y/n)" boolconfigedit
    echo "------------------------------------------------------"
    if [[ $boolconfigedit == "y" ]] || [[ $boolconfigedit == "Y" ]];
        then
            echo "Launching text editor"
            echo "---------------------"
            nano /root/.config/plexscript/plexscript.conf
            functionLoadConfigVariables
            functionPrintConfig
        fi
}
#--------------------------------------------------------------------------------------------------------------------
function functionCreateConfig () {
    echo "Creating config at $configfilelocation"
    echo "-------------------------------------------------------------"
    echo "
    # Stores the script config file location
            confconfigfilelocation=$configfilelocation
    # Stores the cron time code for reboots
            confcronreboottime=$cronreboottime
    # Stores the con time code for updates
            confcronupdateappsostime=$cronupdateappsostime
    # Stores the time zone for the system
            confnewtimezone=$newtimezone
    # Stores the New hostname of system
            varnewhostname=$newhostname
    # Stores the Location of the rclone service file
            varrcloneservice1=$rcloneservice1
    # Stores the Path of rclone mount + folder -- example: googledrive:/mydata/movies
            varrclonemount1=$rclonemount
    # Stores the Path that rclone will be mounted to -- example:/media/mydata/movies
            varrclonetarget1=$rclonetarget
    # Stores the Location of the rclone service file
            varrcloneservice2=$rcloneservice2
    # Stores the Path of rclone mount + folder -- example: googledrive:/mydata/movies
            varrclonemount2=$rclonemount2
    # Stores the Path that rclone will be mounted to -- example:/media/mydata/movies
            varrclonetarget2=$rclonetarget
    # Stores Various media paths to sync from one rclone mount to another, provided the folders are the same name
            varmediadirectory1=$mediadirectory1
            varmediadirectory2=$mediadirectory2
            varmediadirectory3=$mediadirectory3
            varmediadirectory4=$mediadirectory4
            varmediadirectory5=$mediadirectory5
    " >> $configfilelocation
}
#--------------------------------------------------------------------------------------------------------------------
#stores config data and re-creates config with user data
function functionReloadConfig (){
    read -p "Are you sure you want to rebuild the config? (y/n)" boolconfigreload
    echo    "------------------------------------------------------"
    if [[ $boolconfigreload == "y" ]] || [[ $boolconfigreload == "Y" ]];
        then
            functionLoadConfigVariables #Loading the latest variables
            functionPrintConfig
            cp $configfilelocation $configfilelocation-$starttime.OLD
            echo "Backup Config located at: $configfilelocation-$starttime.OLD"
            echo "------------------------"
            rm $configfilelocation
            functionCreateConfig
#            functionPrintConfig # Function to print Configuration
        else 
            echo "No changes made to configuration file"
            echo "-------------------------------------"
        fi
}
#--------------------------------------------------------------------------------------------------------------------
# Checks if a ConJob to set the timezone exists and to create it if it does not
function functionStartPlexBootCron () {
    if  crontab -l | grep -q '#Starts Plex Media Server on Reboot'
        then 
            echo "No Modifications Made for System Reboot Cronjob"
            echo "-------------------------------------------"
        else 
            echo "Adding Cronjob to Set System Reboot"
            echo "-------------------------------"
            crontab -l > /tmp/plexbootcron
            echo "#Starts Plex Media Server on Reboot" >> /tmp/plexbootcron
            echo "      @reboot service plexmediaserver start">> /tmp/plexbootcron
            crontab /tmp/plexbootcron
            rm /tmp/plexbootcron
        fi
}
#--------------------------------------------------------------------------------------------------------------------
# Checks if a ConJob to set the timezone exists and to create it if it does not
function functionSystemRebootCron () {
    if [[ ! -z $cronreboottime ]];
        then
            if  crontab -l | grep -q '#System Reboot'
                then 
                    echo "No Modifications Made for System Reboot Cronjob"
                    echo "-------------------------------------------"
                else 
                    echo "Adding Cronjob to Set System Reboot"
                    echo "-------------------------------"
                    crontab -l > /tmp/systemrebootcron
                    echo "#System Reboot" >> /tmp/systemrebootcron
                    echo "      $cronreboottime reboot" >> /tmp/systemrebootcron
                    crontab /tmp/systemrebootcron
                    rm /tmp/systemrebootcron
                fi
            fi
}
#--------------------------------------------------------------------------------------------------------------------
# Checks if a ConJob to set the timezone exists and to create it if it does not
function functionTimeZoneCron () {
    if  crontab -l | grep -q '#Set Timezone'
        then 
            echo "No Modifications Made for Timezone Cronjob"
            echo "-------------------------------------------"
        else 
            echo "Adding Cronjob to Set Timezone"
            echo "-------------------------------"
            crontab -l > /tmp/settimezonecron
            echo "#Set Timezone" >> /tmp/settimezonecron
            echo "      @reboot cp /usr/share/zoneinfo/$newtimezone /etc/localtime" >> /tmp/settimezonecron
            crontab /tmp/settimezonecron
            rm /tmp/settimezonecron
        fi
}
#--------------------------------------------------------------------------------------------------------------------
# Checks if a ConJob to Automaticly update the System and Apps exists and to create it if it does not
function functionSystemUpdateCron () {
    if  crontab -l | grep -q '#Update Apps and OS'
        then 
            echo "No Modifications Made for System Update Cronjob"
            echo "-------------------------------------------"
        else 
            echo "Adding Cronjob to Automaticly update the System and Apps"
            echo "-------------------------------"
            crontab -l > /tmp/SystemUpdatecron
            echo "#Update Apps and OS" >> /tmp/SystemUpdatecron
            echo "      0 6 * * * apt-get update && apt-get upgrade -f --assume-yes" >> /tmp/SystemUpdatecron
            crontab /tmp/SystemUpdatecron
            rm /tmp/SystemUpdatecron
        fi
}
#--------------------------------------------------------------------------------------------------------------------
function functioninstallplexmediaserver () {
    #checking if apt-transport-https is installed
    apttransporthttpsinstallstatus=$(dpkg-query -W --showformat='${Status}\n' apt-transport-https 2>/dev/null)
    if [[ "x${apttransporthttpsinstallstatus}" != "xinstall ok installed" ]]; 
        then 
            if apt-get -qq install -y apt-transport-https >/dev/null; 
                then
                    sudo apt update
                    echo "apt-transport-https installed"
                    echo "-----------------------------"
                    fi
            fi
    #checking if Plex Media Server is installed
    plexmediaserverinstallstatus=$(dpkg-query -W --showformat='${Status}\n' plexmediaserver 2>/dev/null)
    if [[ "x${plexmediaserverinstallstatus}" != "xinstall ok installed" ]]; 
        then
            sudo apt update
            curl https://downloads.plex.tv/plex-keys/PlexSign.key | sudo apt-key add -
            echo deb https://downloads.plex.tv/repo/deb public main | sudo tee /etc/apt/sources.list.d/plexmediaserver.list
            sudo apt update
            if apt-get -qq install -y plexmediaserver >/dev/null; 
                then
                    echo "Plex Media Server installed"
                    echo "---------------------------"
                    sudo systemctl enable plexmediaserver.service
                    sudo systemctl start plexmediaserver.service
                    systemctl status plexmediaserver
                    fi  
            fi
}
#--------------------------------------------------------------------------------------------------------------------
function functioninstallfirewall () {
    #checking if Fail2Ban is installed
    fail2baninstallstatus=$(dpkg-query -W --showformat='${Status}\n' fail2ban 2>/dev/null)
    if [[ "x${fail2baninstallstatus}" != "xinstall ok installed" ]]; 
        then 
            if apt-get -qq install -y fail2ban >/dev/null; 
                then
                    echo "Fail2Ban installed"
                    echo "------------------"
                    fi
            fi
    #checking if UFW Firewall is installed
    ufwinstallstatus=$(dpkg-query -W --showformat='${Status}\n' ufw 2>/dev/null)
    if [[ "x${ufwinstallstatus}" != "xinstall ok installed" ]]; 
        then 
            if apt-get -qq install -y ufw >/dev/null; 
                then
                    sudo ufw status verbose
                    sudo ufw allow ssh
                    sudo ufw allow 32400/tcp
                    # Turning on Firewall
                    sudo ufw enable
                    echo "Firewall installed"
                    echo "------------------"
                    fi    
            fi
}
#--------------------------------------------------------------------------------------------------------------------
# Reads config and asks user if they want to modify it
function functionRcloneConfig() {
    functionLoadConfigVariables
    functionPrintConfig
    read -p "Do you want to edit the config before proceding? (y/n)" boolconfigrcloneedit
    echo "------------------------------------------------------"
    if [[ $boolconfigrcloneedit == "y" ]] || [[ $boolconfigrcloneedit == "Y" ]];
        then
            rclone config
            fi
}
#--------------------------------------------------------------------------------------------------------------------
function functioninstallrclone () {
    # Installing rclone if you want to mount remote storage
    curl https://rclone.org/install.sh | sudo bash
    # Edit Rclone
    functionRcloneConfig
    echo "Adding the Rclone Mount Service"
    echo "-------------------------------"
    if [[ ! -f $rclonetarget ]];
        then
            echo "Creating Rclone Target Directory"
            echo "--------------------------------"
            mkdir -p $rclonetarget
        else
            echo "Rclone Target Directory Exists"
            echo "------------------------------"
            fi   

    if [[ ! -f /etc/systemd/system/$rcloneservice1 ]];
        then
            echo "Creating Rclone Mount Service File"
            echo "-----------------------------"
            ################ https://www.jamescoyle.net/how-to/3116-rclone-systemd-startup-mount-script ################
            echo "# /etc/systemd/system/$rcloneservice1
            [Unit]
            Description=Plex Wasabi (rclone)
            AssertPathIsDirectory=$rclonetarget
            After=plexdrive.service
                
            [Service]
            Type=simple
            ExecStart=/usr/bin/rclone mount \
                    --config=/root/.config/rclone/rclone.conf \
                    --allow-other \
                    --cache-tmp-upload-path=/tmp/rclone/upload \
                    --cache-chunk-path=/tmp/rclone/chunks \
                    --cache-workers=8 \
                    --cache-writes \
                    --cache-dir=/tmp/rclone/vfs \
                    --cache-db-path=/tmp/rclone/db \
                    --no-modtime \
                    --drive-use-trash \
                    --stats=0 \
                    --checkers=16 \
                    --bwlimit=40M \
                    --dir-cache-time=60m \
                    --cache-info-age=60m $rclonemount $rclonetarget
            ExecStop=/bin/fusermount -u $rclonetarget
            Restart=always
            RestartSec=10
                
            [Install]
            WantedBy=default.target" >> /etc/systemd/system/$rcloneservice1
        else
            echo "Rclone Rclone Mount Service File Exists"
            echo "Please make any changes manually by opening:"
            echo "/etc/systemd/system/$rcloneservice1"
            echo "-------------------------------------------"
            fi 
    echo "Starting Rclone Service"
    echo "-----------------------"
    systemctl start $rcloneservice1
    echo "Enabling Rclone Service"
    echo "-----------------------"
    systemctl enable $rcloneservice1
    echo "Printing Mounted File System"
    echo "----------------------------"
    ls -l $rclonetarget
}
#--------------------------------------------------------------------------------------------------------------------
function functionplexsync () {
    if [[ ! $mediadirectory1 = 0 ]]
        then
            echo "---------- sync $mediadirectory1 ----------"
            rclone copy -P "$rclonemount2"$mediadirectory1  "$rclonemount"$mediadirectory1
        fi
    if [[ ! $mediadirectory2 = 0 ]]
        then
            echo "---------- sync $mediadirectory2 ---------"
            rclone copy -P "$rclonemount2"$mediadirectory2 "$rclonemount"$mediadirectory2
        fi
    if [[ ! $mediadirectory3 = 0 ]]
        then
            echo "---------- sync $mediadirectory3 ---------"
            rclone copy -P "$rclonemount2"$mediadirectory3 "$rclonemount"$mediadirectory3
        fi
    if [[ ! $mediadirectory4 = 0 ]]
        then
            echo "---------- sync $mediadirectory4 ---------"
            rclone copy -P "$rclonemount2"$mediadirectory4 "$rclonemount"$mediadirectory4
        fi
    if [[ ! $mediadirectory5 = 0 ]]
        then
            echo "---------- sync $mediadirectory5 ---------"
            rclone copy -P "$rclonemount2"$mediadirectory5 "$rclonemount"$mediadirectory5
        fi
}
#--------------------------------------------------------------------------------------------------------------------
function functionCreateMOTD () {
    plexmotdLocation=/etc/update-motd.d/01-plex-motd
    sudo apt install screenfetch -y
    echo "Disabling all current MOTDs"
    echo "---------------------------"
    sudo chmod -x /etc/update-motd.d/*
    echo "Creating Custom MOTD"
    echo "-----------------"
    wget "https://gitlab.com/mkolakowski/plex/raw/master/01-plex-motd.sh" -O $plexmotdLocation
    chmod +x $plexmotdLocation
}
#--------------------------------------------------------------------------------------------------------------------
# Downloads Fresh script if none present
if [[ ! -f ./plex.sh ]]; 
    then
        echo "Downloading latest release"
        echo "--------------------------"
        wget -q https://gitlab.com/mkolakowski/plex/raw/master/plex.sh
        sudo chmod u+x ./plex.sh
        fi
#--------------------------------------------------------------------------------------------------------------------
# Config Loader/Generator
if [[ ! -f  /root/.config/plexscript/plexscript.conf ]]; 
    then
        if [[ ! -f /root/.config/plexscript ]];
            then
                sudo mkdir -p /root/.config/plexscript
                echo "Creating Config Directory"
                echo "-------------------------"
            fi        
        # Creating config
        functionCreateConfig
        echo "To edit config please run ./plex.sh config"
        echo "-------------------------------------------"
    fi
#--------------------------------------------------------------------------------------------------------------------
# Choice Logic
if [[ "$1" == "install" ]];
    then
        echo "Installing Plex Server"
        echo "----------------------"
        functionReadConfig
        functionupdateosapps
        functioninstallplexmediaserver
        functioninstallfirewall
        functioninstallrclone
        functionTimeZoneCron
        functionSystemUpdateCron
        functionSystemRebootCron
        functionStartPlexBootCron
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "start" ]];
        then
            echo "Starting Plex Server"
            echo "--------------------"
            service plexmediaserver start
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "restart" ]];
        then
            echo "Restarting Plex Server"
            echo "----------------------"
            service plexmediaserver stop
            service plexmediaserver restart
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "update" ]];
        then
            functionupdateosapps
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "updateself" ]];
        then
            upgradescript=true
            functionUpdateScript 
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "stop" ]];
        then
            service plexmediaserver stop
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "config" ]];
        then
            functionReadConfig
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "cron" ]];
        then
            functionReadConfig
            functionTimeZoneCron
            functionSystemUpdateCron
            functionSystemRebootCron
            functionStartPlexBootCron
            echo "Printing Contab"
            echo "---------------"
            sudo crontab -u root -l    
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "motd" ]];
        then
            functionReadConfig
            functionCreateMOTD
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "hostname" ]];
        then
            functionReadConfig
            functionSetHostname
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "backup" ]];
        then
            echo "Backing up the Plex Server"
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "reload" ]];
        then
            functionReloadConfig
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$1" == "sync" ]];
        then
            functionLoadConfigVariables
            functionplexsync
    #--------------------------------------------------------------------------------------------------------------------
    else
        functionLoadConfigVariables
        functionPrintConfig
        echo " "
        echo "No command found. Here is a list of commands:"
        echo " |------------|----------------------------------------------------------------------------------------|"
        echo " | backup     | Performs a backup of the script config and Plex Database if desired                    |"
        echo " | config     | Prints Script Config on screen and gives option to edit in nano                        |"
        echo " | cron       | Checks if Cron Jobs are in place and prints entire root Contab                         |"
        echo " | hostname   | Updates hostname of system to the listed in the configuration file                     |"
        echo " | install    | Performs the install of the Plex Media Server, LetsEncrypt, Fail2Ban and UFW Firewall  |"
        echo " | motd       | Installs a custom MOTD for the/plex controller                                         |"
        echo " | reload     | Renames current config and creates a new config with users variables                   |"
        echo " | restart    | Performs a Restart on the Plex Media Server Service                                    |"
        echo " | restore    | Copies Files from the backup location to restore the Script Config                     |"
        echo " | start      | Performs a manual start of the Plex Media Server                                       |"
        echo " | stop       | Performs a hard stop of the Plex Media Server                                          |"
        echo " | sync       | Performs a sync between the second and first rclone targets                            |"
        echo " | update     | Updates OS and apps                                                                    |"
        echo " | updateself | Deletes and downloads the latest version of this script                                |"
        echo " |------------|----------------------------------------------------------------------------------------|"
        echo " "
    fi
